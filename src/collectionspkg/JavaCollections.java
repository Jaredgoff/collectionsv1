package collectionspkg;

import java.util.*;

public class JavaCollections {

    public static void main(String[] args) {

        //list
        System.out.println("Here is a List Collection");
        List Truck = new ArrayList();
        Truck.add("Sierra");
        Truck.add("F150");
        Truck.add("Ram 1500");
        Truck.add("CyberTruck");
        Truck.add("Tundra");

        for (Object str : Truck) {
            System.out.println((String) str);
        }

        //map
        System.out.println("Here is a Map Collection");
        Map<Integer, String> Trucks = new HashMap();
        Trucks.put(1, "Sierra");
        Trucks.put(2, "F150");
        Trucks.put(3, "Ram 1500");
        Trucks.put(4, "CyberTruck");
        Trucks.put(5, "Tundra");

        System.out.println(Trucks);


        //Set
        System.out.println("Here is a Set Collection");
        TreeSet<String> setTruck = new TreeSet<String>();
        setTruck.add("Sierra3");
        setTruck.add("Sierra4");
        setTruck.add("Sierra1");
        setTruck.add("Sierra3");
        setTruck.add("Sierra6");
        setTruck.add("Sierra4");
        setTruck.add("Sierra2");
        setTruck.add("Sierra6");
        setTruck.add("Sierra7");
        System.out.println(setTruck);

        //Queue
        System.out.println("Here is a Queue Collection");
        Queue<String> classictrucks = new LinkedList<>();

        classictrucks.add("Landcrusier");
        classictrucks.add("K5 Blazer");
        classictrucks.add("Scout");
        classictrucks.add("Jeep");
        classictrucks.add("Bronco");

        classictrucks.forEach(name -> {
            System.out.println(name);
        });

        //Tree
        System.out.println("Here is my Tree collection using Trucks");
        TreeSet truck = new TreeSet();
        Truck.add("GMC");
        Truck.add("Ford");
        Truck.add("Toyota");
        Truck.add("Chevrolet");
        Truck.add("Dodge");
        Truck.add("Nissan");
        Iterator list = Truck.iterator();
        while(list.hasNext()) {
        Object element = list.next();
        System.out.print(element + "\n");
        }
    }
}






